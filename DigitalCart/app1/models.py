from django.db import models
import datetime

class Category(models.Model):
    name=models.CharField(max_length=50)
    def __str__(self):
        return self.name

class Customer(models.Model):
    fname=models.CharField(max_length=50)
    lname=models.CharField(max_length=50)
    email=models.CharField(max_length=50)
    phoneN=models.IntegerField(max_digits=20)
    password=models.CharField(max_length=20)

    def __str__(self):
        return f"{self.fname } {self.lname}"


class Product(models.Model):
    name=models.CharField(max_length=50)
    price=models.DecimalField(default=0,decimal_places=2,max_digits=7)
    description=models.CharField(max_length=250)
    category=models.ForeignKey(Category,on_delete=models.CASCADE,default=1)
    image=models.ImageField(upload_to='uploads/Product/')
    
    def __str__(self):
       return self.name
class Order(models.Model):
    product=models.ForeignKey(Product,on_delete=models.CASCADE)
    customer=models.ForeignKey(Customer,on_delete=models.CASCADE)
    address=models.CharField(max_length=50,blank=True)
    quantity=models.IntegerField(default=1) 
    phoneN=models.IntegerField(max_length=20)
    date=models.DateField(default=datetime.datetime.today)
    status=models.BooleanField(default=False)
    def __str__(self):
        return self.product


