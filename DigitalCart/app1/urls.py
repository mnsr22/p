from django.urls import path
from . import views

urlpatterns=[

    path('home/',views.Home,name='Home'),
    # path('signin/',views.Signin),
    # path('signup/',views.Signup),
    path('login/',views.Login_register,name='login'),
    path('sell/',views.Sell,name='sell'),
]